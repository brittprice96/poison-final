using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Wizardcontroller :MonoBehaviour
{
    public int speed = 5;
    public int jumpForce = 600;
    private Rigidbody2D rb;
    private Animator _animator;
    public bool grounded;
    public Transform feet;
    public LayerMask groundLayer;
    private int level = 0;
   public GameObject BulletObject;
   public Transform FirePos;
//    public PhotonView photonView;
   public Animator anim;
public SpriteRenderer sr;
  public GameObject bulletPrefab;
   public Image FillImage;
   private bool Dead=false;
    // Start is called before the first frame update
    void Start() {
        rb = GetComponent<Rigidbody2D>();//cached reference to player 1
        _animator = GetComponent<Animator>();
        InvokeRepeating("Firebullet", 2.0f, 0.3f);
    }

IEnumerator DestoryDead() 
{
  yield return new WaitForSeconds(2);
          Destroy(this.gameObject);
}
    // Update is called once per frame
    void Update()
    {
      
     if (FillImage.fillAmount<=0){
         _animator.SetBool("Dead",true);
         Dead = true;
  
        }
        if (Dead){
        StartCoroutine(DestoryDead());
    }
    }

    private void Firebullet()
    {
      if (Dead ==false){
          _animator.SetBool("Grounded",grounded);
            Instantiate(bulletPrefab, FirePos.transform.position, Quaternion.identity);
  
      }
    }
    // private void CheckInput()
    // if(Input.GetKey(KeyCode.Mouse1))
    // shoot();

// private void Shoot()
// {

// if(sr.flipx == false)
// {
//     GameObject obj = PhotonNetwork.Instantiate(BulletObject.name, new Vector2(FirePos.transform.position.x, FirePos.transform.position.y), Quaternion.identity,0);
// }
// if(sr.flipx == true)
// {
//     GameObject obj = PhotonNetwork.Instantiate(BulletObject.name, new Vector2(FirePos.transform.position.x, FirePos.transform.position.y), Quaternion.identity,0);
//  obj.GetComponent<PhotonView>().RPC("ChangeDir_left", PhotonTargets.AllBuffered);   
//     anim.SetTrigger("Wizard Shoot");
// }
//     private void OnCollisionEnter2D(Collision2D collision)
//     {
//         //add condition for the enemy being dead
//         if (other.gameObject.CompareTag("Wall") && level == 1)
//         {
//             SceneManager.LoadScene("Level2");
//             level++;
//         }
//     }
// }
}
