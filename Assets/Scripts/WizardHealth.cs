using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class WizardHealth : MonoBehaviour
{

    public Image FillImage;
private Animator _animator;
    public bool Harm;

    public void ReduceHealth()
    {
        FillImage.fillAmount-=0.1f;
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.CompareTag("PlayerBullet"))
        {
            Destroy(collision.gameObject);  
            ReduceHealth();   
        }
    }
    private void OnCollisionEnter2D(Collision2D other) {
       if(other.gameObject.tag=="Obstacle")
        {
            Destroy(other.gameObject);  
      ReduceHealth(); 
      Debug.Log("hitSpike");
    }
    }
    public void Update() {
    }
}