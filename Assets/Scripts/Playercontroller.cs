using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Playercontroller : MonoBehaviour
{
    public int speed = 5;
    public int jumpForce = 900;
    private Rigidbody2D rb;
    private Animator _animator;
    public bool grounded;
    public Transform feet;
    public LayerMask groundLayer;
    private AudioSource _audio;
    public AudioClip shootSnd;
    public AudioClip jumpSnd;
    public GameObject bulletPrefab;
    public Image Health;
    public Text LivesText;
    public Transform PlayerPos;
    public SpriteRenderer sr;
    public Transform PlayerFire;
    public GameObject sword;
    public GameObject shield;
   
    private Health health;
   // Start is called before the first frame update
    void Start() {
        rb = GetComponent<Rigidbody2D>();//cached reference to player 1
        _animator = GetComponent<Animator>();
        _audio = GetComponent<AudioSource>();
    }

    void Update()
    {
        LivesText.text="Lives:"+PublicVars.Lives;
        float xSpeed = Input.GetAxisRaw("Horizontal") * speed;
        rb.velocity = new Vector2(xSpeed,rb.velocity.y);
        _animator.SetFloat("Speed", Mathf.Abs(xSpeed));

        if (xSpeed <0){
            sr.flipX=true;
        }
        else {
            sr.flipX=false;
        }
        
        grounded = Physics2D.OverlapCircle(feet.position, 0.5f, groundLayer);

        if (Input.GetButtonDown("Jump") && grounded)
        {
            rb.AddForce(new Vector2(0,jumpForce));
            _audio.PlayOneShot(jumpSnd);
        }
        _animator.SetBool("Grounded",grounded);
       
        Firebullet();
        MinusLives();
        CheckLoss();
       if (PublicVars.HaveSword){ 
        sword.SetActive(true);
    }
     if (PublicVars.HaveShield){ 
        shield.SetActive(true);
    }
    }

private void MinusLives(){
if (Health.fillAmount<=0)
{
    PublicVars.Lives--;
_animator.SetBool("Dead",true);
       Revive();
}
}
private void Revive(){
 _animator.SetBool("Dead",false);
    Health.fillAmount=1;
 transform.position=new Vector3(PlayerPos.position.x,PlayerPos.position.y,PlayerPos.position.z);
PublicVars.HealthAmount=1;
}

    private void CheckLoss(){
     if (PublicVars.Lives==0)
        {
SceneManager.LoadScene("Gameover");
PublicVars.Lives=3;
        }
    }
private void Firebullet()
    {
        if (Input.GetKeyDown(KeyCode.F)){
            if (PublicVars.HaveSword)
            {
                
            }
        Instantiate(bulletPrefab, PlayerFire.transform.position, Quaternion.identity);
        Debug.Log("PlayerFire");
        }
    }
        private void OnCollisionEnter2D(Collision2D other)
    {
            //add condition for the enemy being dead
            if (other.gameObject.CompareTag("Door"))
            {
         Scene currentScene = SceneManager.GetActiveScene ();
         if (currentScene.name == "Level1")
         {
                SceneManager.LoadScene("Level2");
            }
             if (currentScene.name == "Level2")
         {
               
                SceneManager.LoadScene("Level3");
                
            }
             if (currentScene.name == "Level3")
         {
            
                SceneManager.LoadScene("Win");
            }
       }
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Sword"))
        {
            Destroy(other.gameObject);
            // PublicVars.HaveSword=true;
            sword.SetActive(true); //destroys game object in the next scene
        }

        if (other.gameObject.CompareTag("Shield"))
        {
            Destroy(other.gameObject);
            shield.SetActive(true);
        }
    }
}
