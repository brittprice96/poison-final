using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//shield works: https://www.youtube.com/watch?v=ZQT7ltoqsAw
public class Health : MonoBehaviour
{

    public Image FillImage;

    public bool Harm;
    private AudioSource _audio;
    public AudioClip hurtSnd;
    private bool shielded;
    [SerializeField] private GameObject shield;
    
    private Playercontroller player;
     public void Start() {
       _audio = GetComponent<AudioSource>();
       shielded = false;
     }

     public void Update() {
         FillImage.fillAmount = PublicVars.HealthAmount;
         CheckShield();
     }
     
     void CheckShield()
     {
         if (Input.GetKey(KeyCode.S) && !shielded)
         {
             shield.SetActive(true);
             shielded = true;
        
             //turn off shield
             Invoke("NoShield", 3f);
         }
     }

     void NoShield()
     {
         shield.SetActive(false);
         shielded = false;
     }
public void ReduceHealth()
{
        PublicVars.HealthAmount-=0.1f;
        _audio.PlayOneShot(hurtSnd);
}


        private void OnTriggerEnter2D(Collider2D collision) {
            if (collision.CompareTag("Bullet"))
            {
                Destroy(collision.gameObject);
           
                if (!shielded)
                {
                    ReduceHealth();  
                    Debug.Log("protect");
                } 
            }
        }
        private void OnCollisionEnter2D(Collision2D other) {
           if(other.gameObject.tag=="Obstacle")
            {
                Destroy(other.gameObject);  
                ReduceHealth();
                
        }
    }
}
