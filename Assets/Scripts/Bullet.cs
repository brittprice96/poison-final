using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bullet : MonoBehaviour
{
    public bool MoveDir = false; //false (right), true (left)
    public float Movespeed;
    public float DestroyTime;
    public bool HarmHealth=false;
    public float BulletDistance;
    private void Awake() {
        StartCoroutine("DestroyByTime");
    }
    public void DestroyObject(){
        Destroy(this.gameObject);
    }

    IEnumerator DestroyByTime()
    {
        yield return new WaitForSeconds(DestroyTime);
        DestroyObject ();
    }

    // [PunRPC]
    // public void ChangeDir_left()
    // {
    //     MoveDir = true;
    // }
    // [PunRPC]
    // public void DestroyObject()
    // {
    //     Destroy(this.GameObject);
    // }

    private void Update() {
        Vector3 Move=Vector3.left*Movespeed*Time.deltaTime;
        Move+=Vector3.left*BulletDistance*Time.deltaTime;
        transform.position+=Move;
    }
}
