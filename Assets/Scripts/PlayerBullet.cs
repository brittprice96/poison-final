using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBullet : MonoBehaviour
{
      public bool MoveDir = false; //false (right), true (left)
    public float Movespeed;
    public float DestroyTime;
    public bool HarmHealth=false;
    private void Awake() {
        StartCoroutine("DestroyByTime");
    }
    public void DestroyObject(){
    Destroy(this.gameObject);
    }

    IEnumerator DestroyByTime()
    {
        yield return new WaitForSeconds(DestroyTime);
        DestroyObject ();
    }

    private void Update() {
        transform.Translate(Vector2.right*Movespeed*DestroyTime*Time.deltaTime);
    }
}
