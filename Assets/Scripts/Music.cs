using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//reference: https://www.youtube.com/watch?v=JKoBWBXVvKY

public class Music : MonoBehaviour {
    private void Awake() {

        GameObject[] objects = GameObject.FindGameObjectsWithTag("Music");
        if (objects.Length > 1) {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
    }
}
